set DATESTYLE to "DMY";

-- DROP TABLE play, client, perfomance, sector, hall, line, ticket, actor, part, casting;
-- DROP TYPE ezone, estatus;

CREATE TABLE play (
  play_id SMALLSERIAL PRIMARY KEY,
  name VARCHAR(256) NOT NULL,
  duration INTERVAL NULL,
  producer VARCHAR(40) NULL,
  info VARCHAR(5000) NULL
);

CREATE TABLE client (
  client_id SERIAL PRIMARY KEY,
  email VARCHAR(30) NOT NULL UNIQUE,
  username VARCHAR(30) NULL,
  pswd_hash CHAR(34) NOT NULL,
  salt CHAR(15) NOT NULL UNIQUE
);

CREATE TABLE perfomance (
  perfomance_id SMALLSERIAL PRIMARY KEY,
  play_id INT NOT NULL REFERENCES play ON DELETE CASCADE,
  start_time TIMESTAMP NULL,
  hall_id INT NULL,
  UNIQUE(start_time, hall_id)
);

CREATE TABLE sector (
  sector_id SMALLSERIAL PRIMARY KEY,
  perfomance_id INT NOT NULL REFERENCES perfomance ON DELETE CASCADE,
  price MONEY NULL,
  tag VARCHAR(20) NULL
);

CREATE TABLE hall (
  hall_id SMALLSERIAL PRIMARY KEY,
  theatre VARCHAR(50) NOT NULL,
  name VARCHAR(20) NOT NULL,
  volume INT NULL,
  UNIQUE(theatre, name)
);

CREATE TYPE ezone 
AS ENUM ('parterre', 'amphitheater', 'lodge',
         'mezzanine', 'balcony', 'sofa');

CREATE TABLE line (
  line_id SMALLSERIAL PRIMARY KEY,
  hall_id INT NOT NULL REFERENCES hall ON DELETE CASCADE,
  line_number INT NOT NULL,
  places INT NULL,
  zone ezone NULL,
  UNIQUE (hall_id, line_number)
);

CREATE TYPE estatus
AS ENUM ('reserved_by_owner', 'free', 'reserved', 'paid');

CREATE TABLE ticket (
  ticket_id SERIAL PRIMARY KEY,
  perfomance_id INT NOT NULL REFERENCES perfomance ON DELETE CASCADE,
  sector_id INT NULL REFERENCES sector ON DELETE SET NULL,
  line_id INT NOT NULL REFERENCES line ON DELETE CASCADE,
  place SMALLINT NOT NULL,
  status estatus DEFAULT 'free' NULL,
  purchase_price MONEY NULL,
  client_id INT NULL REFERENCES client,
  code INT NOT NULL,
  reservation_time TIMESTAMP NULL,
  UNIQUE(perfomance_id, line_id, place)
);

CREATE TABLE actor (
  actor_id SMALLSERIAL PRIMARY KEY,
  name VARCHAR(40) NOT NULL
);

CREATE TABLE part (
  part_id SMALLSERIAL PRIMARY KEY,
  play_id INT NOT NULL REFERENCES play ON DELETE CASCADE,
  name VARCHAR(50) NULL,
  UNIQUE(play_id, name)
);

CREATE TABLE casting (
  perfomance_id INT NOT NULL REFERENCES perfomance ON DELETE CASCADE,
  part_id INT NOT NULL REFERENCES part ON DELETE CASCADE,
  actor_id INT NULL REFERENCES actor ON DELETE SET NULL,
  PRIMARY KEY (perfomance_id, part_id)
);
