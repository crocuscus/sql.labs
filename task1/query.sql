WITH useful_department AS 
	(SELECT department_id 
	 FROM location NATURAL JOIN department  
	 WHERE regional_group = 'DALLAS')
SELECT last_name, middle_initial, first_name, function, department_id
FROM employee NATURAL JOIN job NATURAL JOIN useful_department
where function = 'MANAGER';

