SELECT manager.first_name, manager.middle_initial, manager.last_name,
FROM employee AS manager JOIN (employee NATURAL JOIN job) AS inferior
ON manager.employee_id = inferior.manager_id
WHERE function = 'ANALYST' and manager.hire_date + INTERVAL '5 year' > inferior.hire_date;
