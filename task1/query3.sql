SELECT AVG(total), EXTRACT(MONTH FROM order_date) as month
FROM sales_order
GROUP BY month
ORDER BY month ASC;