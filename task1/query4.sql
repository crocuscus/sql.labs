SELECT description as name, sum(total) as money
FROM item NATURAL JOIN product NATURAL JOIN sales_order
WHERE EXTRACT(YEAR from order_date) = 1989
GROUP BY product_id, description
ORDER BY money DESC